package com.fingit.lotaltyapp.interfaces.views;

import com.fingit.lotaltyapp.models.Clients;

import java.util.List;

public interface ClientsViewInterface {
    void showLoading();
    void hideLoading();
    void onResultGetClients(List<Clients.Client> clients);
    void fillInputs(Clients.Client client);
    void onErrorLoading(String message);
    void clearInputs();
    void goToRegisterPoints();
}

