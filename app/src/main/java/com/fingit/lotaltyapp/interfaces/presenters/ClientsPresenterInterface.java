package com.fingit.lotaltyapp.interfaces.presenters;

import com.fingit.lotaltyapp.models.Clients;

import java.util.List;

public interface ClientsPresenterInterface {
    void onResultGetClients(List<Clients.Client> clients);
}
