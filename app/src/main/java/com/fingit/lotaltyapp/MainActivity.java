package com.fingit.lotaltyapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.fingit.lotaltyapp.activities.ClientsActivity;
import com.fingit.lotaltyapp.activities.ConsultPointsActivity;
import com.fingit.lotaltyapp.activities.ScannerCodeActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.samples.vision.barcodereader.R;
import com.google.android.gms.vision.barcode.Barcode;

public class MainActivity extends AppCompatActivity {


    private LinearLayout addPointsManually, addPointsBarcodeScanner, changeScannerCode, consultPoints;
    private static final int RC_BARCODE_CAPTURE = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addPointsManually = findViewById(R.id.add_points_manually);
        addPointsBarcodeScanner = findViewById(R.id.add_points_barcode_scanner);
        changeScannerCode = findViewById(R.id.change_scanner_code);
        consultPoints = findViewById(R.id.consult_points);

        addPointsManually.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ClientsActivity.class)));
        addPointsBarcodeScanner.setOnClickListener(view -> {
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
            intent.putExtra(BarcodeCaptureActivity.UseFlash, true);

            startActivityForResult(intent, RC_BARCODE_CAPTURE);

        });
        changeScannerCode.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ScannerCodeActivity.class)));
        consultPoints.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ConsultPointsActivity.class)));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Intent intent = new Intent(this, ClientsActivity.class);
                    intent.putExtra(BarcodeCaptureActivity.BarcodeObject, barcode);
                    startActivity(intent);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
