package com.fingit.lotaltyapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Clients {

    @SerializedName("clients")
    @Expose
    private List<Client> clients = null;

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }


    public class Timestamp {

        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }



    public static class Client {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("identity_card")
        @Expose
        private String identityCard;
        @SerializedName("names")
        @Expose
        private String names;
        @SerializedName("surnames")
        @Expose
        private String surnames;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("birthdate")
        @Expose
        private String birthdate;
        @SerializedName("birthplace")
        @Expose
        private String birthplace;
        @SerializedName("points")
        @Expose
        private Integer points;
        @SerializedName("timestamp")
        @Expose
        private Timestamp timestamp;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIdentityCard() {
            return identityCard;
        }

        public void setIdentityCard(String identityCard) {
            this.identityCard = identityCard;
        }

        public String getNames() {
            return names;
        }

        public void setNames(String names) {
            this.names = names;
        }

        public String getSurnames() {
            return surnames;
        }

        public void setSurnames(String surnames) {
            this.surnames = surnames;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getBirthdate() {
            return birthdate;
        }

        public void setBirthdate(String birthdate) {
            this.birthdate = birthdate;
        }

        public String getBirthplace() {
            return birthplace;
        }

        public void setBirthplace(String birthplace) {
            this.birthplace = birthplace;
        }

        public Integer getPoints() {
            return points;
        }

        public void setPoints(Integer points) {
            this.points = points;
        }

        public Timestamp getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Timestamp timestamp) {
            this.timestamp = timestamp;
        }

    }
}
