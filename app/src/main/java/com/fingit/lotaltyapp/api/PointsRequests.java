package com.fingit.lotaltyapp.api;

import com.fingit.lotaltyapp.models.Clients;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PointsRequests {

    @FormUrlEncoded
    @POST("api/clients")
    Call<Object> registerPoints();
}
