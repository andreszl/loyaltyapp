package com.fingit.lotaltyapp.api;

import com.fingit.lotaltyapp.models.Clients;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ClientsRequests {

    @GET("api/clients")
    Call<Clients> getClients();

    @GET("api/clients/getClientByIdentityCard/{identity_card}")
    Call<Clients> getClientByIdentityCard(@Path("identity_card") String category);

    @FormUrlEncoded
    @POST("api/clients")
    Call<Clients> saveClient(
        @Field("identity_card") String identity_card,
        @Field("names") String names,
        @Field("surnames") String surnames,
        @Field("gender") String gender,
        @Field("birthdate") String birthdate,
        @Field("birthplace") String birthplace
    );

    @FormUrlEncoded
    @POST("api/clients/registerPointsToClientByIdentityCard")
    Call<Clients> registerPointsToClientByIdentityCard(
        @Field("identity_card") String identity_card,
        @Field("points") int points
    );

}

