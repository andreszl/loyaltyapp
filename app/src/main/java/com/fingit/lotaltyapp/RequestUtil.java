package com.fingit.lotaltyapp;

import android.app.AlertDialog;
import android.content.Context;

import com.fingit.lotaltyapp.api.Api;
import com.fingit.lotaltyapp.api.ClientsRequests;
import com.fingit.lotaltyapp.api.PointsRequests;


public class RequestUtil {

    public static ClientsRequests getClientsApi() {
        return Api.getApi().create(ClientsRequests.class);
    }

    public static PointsRequests getPointsApi() {
        return Api.getApi().create(PointsRequests.class);
    }

    public static AlertDialog showDialogMessage(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).setTitle(title).setMessage(message).show();
        if (alertDialog.isShowing()) {
            alertDialog.cancel();
        }
        return alertDialog;
    }
}