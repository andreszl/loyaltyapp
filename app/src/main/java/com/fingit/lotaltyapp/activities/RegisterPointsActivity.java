package com.fingit.lotaltyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fingit.lotaltyapp.MainActivity;
import com.fingit.lotaltyapp.interfaces.views.PointsViewInterface;
import com.fingit.lotaltyapp.presenters.PointsPresenter;
import com.google.android.gms.samples.vision.barcodereader.R;

public class RegisterPointsActivity extends AppCompatActivity implements PointsViewInterface {

    private String identityCard, names, surnames;
    private TextView identityCardClient, fullNameClient,
            tvCurrentPoints, tvPointsValueCurrentPoints,
            tvNewPoints, tvPointsValueNewPoints,
            tvUsedPoints, tvPointsValueUsedPoints,
            tvTotalPoints, tvPointsValueTotalPoints,
            tvTotalToPay;

    private EditText priceInput, pointsInput;

    private Button btnRegisterPoints;

    private int pointValue = 1200;
    private int currentPoints;
    private int moneyValueForGetPoint = 8320;

    PointsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_points);

        pointsInput = findViewById(R.id.points);
        priceInput = findViewById(R.id.price);

        tvCurrentPoints = findViewById(R.id.current_points);
        tvPointsValueCurrentPoints = findViewById(R.id.points_value_current_points);
        tvNewPoints = findViewById(R.id.new_points);
        tvPointsValueNewPoints = findViewById(R.id.points_value_new_points);
        tvUsedPoints = findViewById(R.id.used_points);
        tvPointsValueUsedPoints = findViewById(R.id.points_value_used_points);
        tvTotalPoints = findViewById(R.id.total_points);
        tvPointsValueTotalPoints = findViewById(R.id.points_value_total_points);
        tvTotalToPay = findViewById(R.id.total_to_pay);

        identityCardClient = findViewById(R.id.identity_card_client);
        fullNameClient = findViewById(R.id.full_name_client);

        btnRegisterPoints = findViewById(R.id.register_points);

        initialize();


        priceInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                String price = priceInput.getText().toString();

                if (!price.isEmpty()) {
                    int newPoints = ((Integer.parseInt(price) * 1)/moneyValueForGetPoint);
                    tvNewPoints.setText(Integer.toString(newPoints));

                    float pointsValueNewPoints = newPoints * pointValue;
                    tvPointsValueNewPoints.setText(Float.toString(pointsValueNewPoints));
                } else {
                    tvNewPoints.setText("0");
                    tvPointsValueNewPoints.setText("0");
                }

                calculateRemainingPoints();
                calculateToltalToPay();
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
        });

        pointsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                String points = pointsInput.getText().toString();

                if (!points.isEmpty()) {
                    tvUsedPoints.setText(points);

                    float pointsValueUsedPoints = Integer.parseInt(points) * pointValue;
                    tvPointsValueUsedPoints.setText(Float.toString(pointsValueUsedPoints));
                } else {
                    tvUsedPoints.setText("0");
                    tvPointsValueUsedPoints.setText("0");
                }

                calculateRemainingPoints();
                calculateToltalToPay();
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
        });

        presenter = new PointsPresenter(this);

        btnRegisterPoints.setOnClickListener(view -> {
            int points = Integer.parseInt(tvTotalPoints.getText().toString()) + Integer.parseInt(tvNewPoints.getText().toString());
            presenter.registerPointsToClientByIdentityCard(identityCard, points);
        });

    }

    void initialize() {
        Intent intent = getIntent();

        identityCard = intent.getStringExtra("identity_card");
        names = intent.getStringExtra("names");
        surnames = intent.getStringExtra("surnames");
        currentPoints = intent.getIntExtra("current_points", 0);

        identityCardClient.setText(identityCard);
        fullNameClient.setText(names + " " + surnames);
        tvCurrentPoints.setText(Integer.toString(currentPoints));

        float pointsValue = currentPoints * pointValue;
        tvPointsValueCurrentPoints.setText(Float.toString(pointsValue));

        tvNewPoints.setText("0");
        tvPointsValueNewPoints.setText("0");
        tvUsedPoints.setText("0");
        tvPointsValueUsedPoints.setText("0");
        tvTotalToPay.setText("0");

        calculateRemainingPoints();
    }

    void calculateRemainingPoints() {
        String points = pointsInput.getText().toString();

        if (points.isEmpty()) {
            points = "0";
        }

        int remainingPoints = currentPoints - Integer.parseInt(points);

        tvTotalPoints.setText(Integer.toString(remainingPoints));

        float pointsValueTotalPoints = remainingPoints * pointValue;
        tvPointsValueTotalPoints.setText(Float.toString(pointsValueTotalPoints));
    }

    void calculateToltalToPay() {
        float totalToPay = Float.parseFloat(priceInput.getText().toString()) - Float.parseFloat(tvPointsValueUsedPoints.getText().toString());
        tvTotalToPay.setText(Float.toString(totalToPay));
    }

    @Override
    public void goToHome() {
        Intent intent = new Intent(RegisterPointsActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
