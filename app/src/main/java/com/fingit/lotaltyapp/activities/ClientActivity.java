package com.fingit.lotaltyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.fingit.lotaltyapp.BarcodeCaptureActivity;
import com.fingit.lotaltyapp.MainActivity;
import com.google.android.gms.samples.vision.barcodereader.R;
import com.google.android.gms.vision.barcode.Barcode;

public class ClientActivity extends AppCompatActivity {

    private int pointValue = 1200;
    private int currentPoints;
    TextView tvCurrentPoints, tvPointsValueCurrentPoints, tvFullName, tvIdentityCard;

    private String identityCard, names, surnames;

    Button btnGoToHome, btnGoBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        tvCurrentPoints = findViewById(R.id.current_points);
        tvPointsValueCurrentPoints = findViewById(R.id.points_value_current_points);
        tvIdentityCard = findViewById(R.id.identity_card_client);
        tvFullName = findViewById(R.id.full_name_client);
        btnGoBack = findViewById(R.id.btn_go_back);
        btnGoToHome = findViewById(R.id.btn_go_home);

        Intent intent = getIntent();

        identityCard = intent.getStringExtra("identity_card");
        names = intent.getStringExtra("names");
        surnames = intent.getStringExtra("surnames");
        currentPoints = intent.getIntExtra("current_points", 0);

        tvIdentityCard.setText(identityCard);
        tvFullName.setText(names + " " + surnames);
        tvCurrentPoints.setText(Integer.toString(currentPoints));

        float pointsValue = currentPoints * pointValue;
        tvPointsValueCurrentPoints.setText(Float.toString(pointsValue));

        btnGoBack.setOnClickListener(view -> finish());

        btnGoToHome.setOnClickListener(view -> startActivity(new Intent(ClientActivity.this, MainActivity.class)));

    }
}
