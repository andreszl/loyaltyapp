package com.fingit.lotaltyapp.activities;

public class CustomItemSpinner {

    private String spinnerItemName;

    public CustomItemSpinner(String spinnerItemName) {
        this.spinnerItemName = spinnerItemName;
    }

    public String getSpinnerItemName() {
        return spinnerItemName;
    }
}
