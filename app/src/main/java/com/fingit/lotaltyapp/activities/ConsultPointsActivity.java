package com.fingit.lotaltyapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fingit.lotaltyapp.BarcodeCaptureActivity;
import com.fingit.lotaltyapp.interfaces.presenters.ClientsPresenterInterface;
import com.fingit.lotaltyapp.models.Clients;
import com.fingit.lotaltyapp.presenters.ClientsPresenter2;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.samples.vision.barcodereader.R;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsultPointsActivity extends AppCompatActivity implements ClientsPresenterInterface {

    ClientsPresenter2 clientsPresenter2;

    TextView barcodeScanner, resetFormLink;
    AutoCompleteTextView identityCardInput;
    Button btnConfirm;

    private static final int RC_BARCODE_CAPTURE = 9001;


    public List<Clients.Client> clients;
    public Clients.Client selectedClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_points);

        barcodeScanner = findViewById(R.id.read_barcode);
        identityCardInput = findViewById(R.id.identity_Card);
        btnConfirm = findViewById(R.id.btn_confirm);
        resetFormLink = findViewById(R.id.reset_form_link);


        barcodeScanner.setOnClickListener(view -> {
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
            intent.putExtra(BarcodeCaptureActivity.UseFlash, true);

            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        });

        resetFormLink.setOnClickListener(view -> identityCardInput.setText(""));


        clientsPresenter2 = new ClientsPresenter2(this);
        clientsPresenter2.getClients();


        btnConfirm.setOnClickListener(view -> {

            checkClient();
        });

    }

    public int findClientPositionByIdentityCard(String identityCard) {
        for (int position=0; position<clients.toArray().length; position++)
            if (clients.get(position).getIdentityCard().equals(identityCard))
                return position;
        return -1;
    }

    @Override
    public void onResultGetClients(List<Clients.Client> clients) {
        this.clients = clients;

        String[] identityCards = new String[clients.toArray().length];

        for (int i = 0; i < clients.toArray().length; ++i) {
            identityCards[i] = clients.get(i).getIdentityCard();
        }

        ArrayAdapter<String> adapter =  new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, identityCards);
        identityCardInput.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);

                    if (barcode != null) {
                        Log.d("Real Barcode", barcode.displayValue.substring(0, 200));

                        String formatedBarcodeValue = Pattern.compile("[^a-zA-Z0-9ñ_àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ+-|].*?\\b", Pattern.MULTILINE)
                                .matcher(barcode.displayValue.substring(0, 200))
                                .replaceAll("|");

                        Log.d("formatedBarcodeValue", formatedBarcodeValue);

                        // find identity card
                        Matcher m = Pattern.compile("([0-9]{10})([a-zA-Z]{3})" ,Pattern.MULTILINE).matcher(formatedBarcodeValue);

                        String identityCard = "";

                        if(m.find()) {
                            identityCard = m.group(1);
                        }

                        identityCardInput.setText(identityCard);

                        checkClient();

                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void checkClient() {
        String identityCard = identityCardInput.getText().toString();

        int position = findClientPositionByIdentityCard(identityCard);

        if (position != -1) {
            Intent intent = new Intent(this, ClientActivity.class);

            intent.putExtra("identity_card", clients.get(position).getIdentityCard());
            intent.putExtra("names", clients.get(position).getNames());
            intent.putExtra("surnames", clients.get(position).getSurnames());
            intent.putExtra("current_points", clients.get(position).getPoints());

            startActivity(intent);
        } else {
            Toast.makeText(this, "Cliente no encontrado", Toast.LENGTH_LONG).show();
        }
    }

}
