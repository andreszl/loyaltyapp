package com.fingit.lotaltyapp.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fingit.lotaltyapp.BarcodeCaptureActivity;
import com.fingit.lotaltyapp.interfaces.views.ClientsViewInterface;
import com.fingit.lotaltyapp.models.Clients;
import com.fingit.lotaltyapp.presenters.ClientsPresenter;
import com.google.android.gms.samples.vision.barcodereader.R;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ClientsActivity extends AppCompatActivity implements ClientsViewInterface {

    ClientsPresenter presenter;

    AutoCompleteTextView identityCardInput;

    EditText namesInput, surnamesInput, birthdateInput, birthplaceInput;

    Spinner genderInput;

    Button btnNext;

    TextView resetFormLink;

    private boolean filled = false;


    public List<Clients.Client> clients;

    String[] genders = new String[]{
        "Seleccione Genero",
        "Masculino",
        "Femenino",
    };

    final List<String> gendersList = new ArrayList<>(Arrays.asList(genders));

    private ArrayAdapter<String> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        resetFormLink = findViewById(R.id.reset_form_link);
        identityCardInput = findViewById(R.id.identity_Card);
        namesInput = findViewById(R.id.names);
        surnamesInput = findViewById(R.id.surnames);
        genderInput = findViewById(R.id.gender);
        birthdateInput = findViewById(R.id.birthdate);
        birthplaceInput = findViewById(R.id.birthplace);
        btnNext = findViewById(R.id.btn_next);

        birthdateInput.setFocusable(false);

        resetFormLink.setOnClickListener(view -> {
            clearInputs();
        });

        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, gendersList) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view;

                if(position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }

                return view;
            }

            @Override
            public boolean isEnabled(int position){
                if(position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }

                return view;
            }
        };

        identityCardInput.setOnItemClickListener((adapterView, view, i, l) -> {
            String identityCard = identityCardInput.getText().toString();

            int position = findClientPositionByIdentityCard(identityCard);

            if (position != -1) {
              fillInputs(clients.get(position));
            }
        });

        identityCardInput.setOnFocusChangeListener((view, focus) -> {
            if(!focus && !filled) {
                String identityCard = identityCardInput.getText().toString();
                presenter.fillInputsIsExistClient(identityCard);
            }
        });

        birthdateInput.setOnClickListener(view -> {

            if(!filled) {
                Calendar calendar = Calendar.getInstance();

                DatePickerDialog dialog = new DatePickerDialog(
                        ClientsActivity.this,
                        R.style.Theme_AppCompat_Light_Dialog_MinWidth,
                        (datePicker, year, month, day) -> birthdateInput.setText(year + "/" + (month+1) + "/" + day),
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }

        });

        btnNext.setOnClickListener(view -> {

            if (identityCardInput.getText().toString().isEmpty()) {
                identityCardInput.setError("Este campo es requerido");
                return;
            }

            if (namesInput.getText().toString().isEmpty()) {
                namesInput.setError("Este campo es requerido");
                return;
            }

            if (surnamesInput.getText().toString().isEmpty()) {
                surnamesInput.setError("Este campo es requerido");
                return;
            }

            if (genderInput.getSelectedItem().toString().equalsIgnoreCase("Seleccione Genero")) {
                ((TextView)genderInput.getSelectedView()).setError("Este Campo es requerido");
                return;
            }

            if (birthdateInput.getText().toString().isEmpty()) {
                birthdateInput.setError("Este campo es requerido");
                return;
            } else {
                birthplaceInput.setError(null, null);
            }

            if (birthplaceInput.getText().toString().isEmpty()) {
                birthplaceInput.setError("Este campo es requerido");
                return;
            }

            int position = findClientPositionByIdentityCard(identityCardInput.getText().toString());

            if (position != -1) {
                goToRegisterPoints();
            } else {
                Clients.Client client = new Clients.Client();

                client.setIdentityCard(identityCardInput.getText().toString());
                client.setNames(namesInput.getText().toString());
                client.setSurnames(surnamesInput.getText().toString());

                String gender = genderInput.getSelectedItem().toString();

                if (gender == "Masculino") {
                    client.setGender("M");
                } else {
                    client.setGender("F");
                }

                client.setBirthdate(birthdateInput.getText().toString());
                client.setBirthplace(birthplaceInput.getText().toString());

                presenter.saveClient(client);
                clients.add(client);
                goToRegisterPoints();
            }

        });

        genderInput.setAdapter(spinnerAdapter);

        presenter = new ClientsPresenter(this);
        presenter.getClients();


        Barcode barcode = getIntent().getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);

        if (barcode != null) {
            Log.d("Real Barcode", barcode.displayValue.substring(0, 200));

            String formatedBarcodeValue = Pattern.compile("[^a-zA-Z0-9ñ_àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ+-|].*?\\b", Pattern.MULTILINE)
                    .matcher(barcode.displayValue.substring(0, 200))
                    .replaceAll("|");

            Log.d("formatedBarcodeValue", formatedBarcodeValue);


            // find identity card
            Matcher m = Pattern.compile("([0-9]{10})([a-zA-Z]{3})" ,Pattern.MULTILINE).matcher(formatedBarcodeValue);

            String identityCard = "";

            if(m.find()) {
                identityCard = m.group(1);
            }

            identityCardInput.setText(identityCard);


            // find all names

            String[] parts = formatedBarcodeValue.split("(F|M)(\\d{1,4})(\\d{1,2})(\\d{1,2}).+?(?=[a-zA-Z])([^\\d]{2})");

            Matcher fullName = Pattern.compile("([a-zA-Z][^PubDSK_1].*(?=[|]))" ,Pattern.MULTILINE).matcher(parts[0]);



            if(fullName.find()) {
                String[] names = fullName.group(0).split("[|]");

                surnamesInput.setText(names[0] + " " + names[1]);

                try {
                    namesInput.setText(names[2] + " " + names[3]);
                } catch(ArrayIndexOutOfBoundsException e) {
                    namesInput.setText(names[2]);
                }
            }


            // find data

            Matcher data = Pattern.compile("(F|M)(\\d{1,4})(\\d{1,2})(\\d{1,2}).+?(?=[a-zA-Z])([^\\d]{2})" ,Pattern.MULTILINE).matcher(formatedBarcodeValue);

            if(data.find()) {

                if (data.group(1).equalsIgnoreCase("M")) {
                    genderInput.setSelection(1);
                } else {
                    genderInput.setSelection(2);
                }

                birthdateInput.setText(data.group(2) + "/" + data.group(3) + "/" + data.group(4));
            }

            presenter.fillInputsIsExistClient(identityCard);

        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onResultGetClients(List<Clients.Client> clients) {

        this.clients = clients;

        String[] identityCards = new String[clients.toArray().length];

        for (int i = 0; i < clients.toArray().length; ++i) {
            identityCards[i] = clients.get(i).getIdentityCard();
        }

        ArrayAdapter<String> adapter =  new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, identityCards);
        identityCardInput.setAdapter(adapter);
    }

    @Override
    public void fillInputs(Clients.Client client) {


        namesInput.setText(client.getNames());
        surnamesInput.setText(client.getSurnames());
        birthdateInput.setText(client.getBirthdate());
        birthplaceInput.setText(client.getBirthplace());

        if (client.getGender().equalsIgnoreCase("M")) {
            genderInput.setSelection(1);
        } else {
            genderInput.setSelection(2);
        }


        genderInput.setFocusable(false);
        genderInput.setEnabled(false);

        namesInput.setFocusable(false);
        namesInput.setEnabled(false);

        surnamesInput.setFocusable(false);
        surnamesInput.setEnabled(false);

        birthdateInput.setEnabled(false);

        birthplaceInput.setFocusable(false);
        birthplaceInput.setEnabled(false);
    }

    @Override
    public void onErrorLoading(String message) {

    }

    @Override
    public void clearInputs() {
        filled = false;

        identityCardInput.setText("");
        namesInput.setText("");
        surnamesInput.setText("");
        birthdateInput.setText("");
        birthplaceInput.setText("");

        genderInput.setSelection(0);

        genderInput.setFocusable(true);
        genderInput.setEnabled(true);


        namesInput.setFocusableInTouchMode(true);
        namesInput.setEnabled(true);

        surnamesInput.setFocusableInTouchMode(true);
        surnamesInput.setEnabled(true);

        birthdateInput.setEnabled(true);

        birthplaceInput.setFocusableInTouchMode(true);
        birthplaceInput.setEnabled(true);
    }

    @Override
    public void goToRegisterPoints() {
        Intent intent = new Intent(ClientsActivity.this, RegisterPointsActivity.class);

        int position = findClientPositionByIdentityCard(identityCardInput.getText().toString());

        if (position != -1) {
            intent.putExtra("current_points", clients.get(position).getPoints());
        }

        intent.putExtra("identity_card", identityCardInput.getText().toString());
        intent.putExtra("names", namesInput.getText().toString());
        intent.putExtra("surnames", surnamesInput.getText().toString());
        startActivity(intent);
    }

    public int findClientPositionByIdentityCard(String identityCard) {
        for (int position=0; position<clients.toArray().length; position++)
            if (clients.get(position).getIdentityCard().equals(identityCard))
                return position;
        return -1;
    }
}
