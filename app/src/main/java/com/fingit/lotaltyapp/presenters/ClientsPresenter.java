package com.fingit.lotaltyapp.presenters;

import android.util.Log;

import androidx.annotation.NonNull;

import com.fingit.lotaltyapp.RequestUtil;
import com.fingit.lotaltyapp.interfaces.views.ClientsViewInterface;
import com.fingit.lotaltyapp.models.Clients;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientsPresenter {

    private ClientsViewInterface view;

    public ClientsPresenter(ClientsViewInterface view) {
        this.view = view;
    }

     public void getClients() {
         Call<Clients> clientsCall = RequestUtil.getClientsApi().getClients();
         clientsCall.enqueue(new Callback<Clients>() {
             @Override
             public void onResponse(@NonNull Call<Clients> call, @NonNull Response<Clients> response) {
                 if (response.isSuccessful() && response.body() != null) {
                     Gson gson = new Gson();
                     String json = gson.toJson(response.body());
                     Log.d("Result", json);
                     view.onResultGetClients(response.body().getClients());
                 }
             }

             @Override
             public void onFailure(@NonNull Call<Clients> call, @NonNull Throwable t) {
                 t.printStackTrace();
             }
         });

    }

    public void fillInputsIsExistClient(String identityCard) {
        Call<Clients> clientsCall = RequestUtil.getClientsApi().getClientByIdentityCard(identityCard);
        clientsCall.enqueue(new Callback<Clients>() {
            @Override
            public void onResponse(@NonNull Call<Clients> call, @NonNull Response<Clients> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getClients().toArray().length > 0) {
                        view.fillInputs(response.body().getClients().get(0));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Clients> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void saveClient(Clients.Client client) {
        Call<Clients> call = RequestUtil.getClientsApi().saveClient(
            client.getIdentityCard(),
            client.getNames(),
            client.getSurnames(),
            client.getGender(),
            client.getBirthdate(),
            client.getBirthplace()
        );

        call.enqueue(new Callback<Clients>() {
            @Override
            public void onResponse(@NonNull Call<Clients> call, @NonNull Response<Clients> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.goToRegisterPoints();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Clients> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
