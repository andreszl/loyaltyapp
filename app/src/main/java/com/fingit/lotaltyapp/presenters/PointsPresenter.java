package com.fingit.lotaltyapp.presenters;

import androidx.annotation.NonNull;

import com.fingit.lotaltyapp.RequestUtil;
import com.fingit.lotaltyapp.interfaces.views.PointsViewInterface;
import com.fingit.lotaltyapp.models.Clients;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointsPresenter {

    private PointsViewInterface view;

    public PointsPresenter(PointsViewInterface view) {
        this.view = view;
    }


    public void registerPointsToClientByIdentityCard(String identityCard, int points) {

        Call<Clients> call = RequestUtil.getClientsApi().registerPointsToClientByIdentityCard(identityCard, points);

        call.enqueue(new Callback<Clients>() {
            @Override
            public void onResponse(@NonNull Call<Clients> call, @NonNull Response<Clients> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.goToHome();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Clients> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
